//
// Created by Konstantin Skornyakov on 07.11.18.
//

#include <fstream>
#include <iostream>
#include <string>
#include <array>
#include <vector>
#include <iterator>
#include <cstring>
#include <limits>
#include <cassert>
#include <thread>
#include <future>
#include <queue>
#include <iomanip>
#include <cstdlib>
#include <xmmintrin.h>


/* TODO:
* Избавиться от создания трэда на каждую итерацию
* 
*/


typedef uint8_t raw;


struct resolution {
	std::uint16_t width;
	std::uint16_t height;
};


struct image {
	std::vector<raw> data;
	resolution res;
};


std::ifstream::pos_type fileSize(const char* filename) {
	std::ifstream in(filename, std::ifstream::ate | std::ifstream::binary);
	return in.tellg();
}


namespace bt601 {

template<typename RET, typename VAL>
inline RET limit_cast(VAL val) {
	static_assert(std::is_integral<RET>::value && std::is_arithmetic<VAL>::value,
		"limit_cast requires first type is_integral and second type is_arithmetic");
	if (val < std::numeric_limits<RET>::min()) { return std::numeric_limits<RET>::min(); }
	if (val > std::numeric_limits<RET>::max()) { return std::numeric_limits<RET>::max(); }
	return static_cast<RET>(val);
}

namespace rgb2yuv {
	const float yRatioR = 0.299;
	const float yRatioB = 0.114;
	const float yRatioG = 0.587;
	
	const float uRatioR = -0.14713;
	const float uRatioG = -0.28886;
	const float uRatioB = 0.436;
	
	const float vRatioR = 0.615;
	const float vRatioG = -0.51499;
	const float vRatioB = -0.10001;
	
	inline raw Y(raw R, raw G, raw B) {
		return limit_cast<raw>(yRatioR * R + yRatioG * G + yRatioB * B);
	}
	
	inline raw U(raw R, raw G, raw B) {
		return limit_cast<raw>(uRatioR * R + uRatioG * G + uRatioB * B + 128);
	}
	
	inline raw V(raw R, raw G, raw B) {
		return limit_cast<raw>(vRatioR * R + vRatioG * G + vRatioB * B + 128);
	}
} // namespace rgb2yuv

namespace yuv2rgb {
	inline raw R(raw Y, raw V) {
		return limit_cast<raw>(Y + 1.13983 * (V - 128));
	}
	
	inline raw G(raw Y, raw U, raw V) {
		return limit_cast<raw>(Y - 0.39465 * (U - 128) - 0.58060 * (V - 128));
	}
	
	inline raw B(raw Y, raw U) {
		return limit_cast<raw>(Y + 2.03211 * (U - 128));
	}
} // namespace yuv2rgb

} // namespace bt601


namespace bmp {

const std::size_t HEADER_SIZE = 54;

inline uint32_t fileSize(const char *header) { return *reinterpret_cast<const uint32_t *>(&header[2]); };
inline uint32_t dataOffset(const char *header) { return *reinterpret_cast<const uint32_t *>(&header[10]); };
inline uint32_t width(const char *header) { return *reinterpret_cast<const uint32_t *>(&header[18]); };
inline uint32_t height(const char *header) { return *reinterpret_cast<const uint32_t *>(&header[22]); };
inline uint16_t depth(const char *header) { return *reinterpret_cast<const uint16_t *>(&header[28]); };

image readFile(const std::string& file, bool swap_RnB = true);
} // namespace bmp


namespace rgb {

const uint8_t pixelSize = 3;

inline uint8_t getR(const raw *point) { return *point; };
inline uint8_t getG(const raw *point) { return *(point + 1); };
inline uint8_t getB(const raw *point) { return *(point + 2); };

} // namespace rgb


namespace convert {

struct memLayout {
	std::size_t pixels;
	std::size_t rgbSize;
	std::size_t frameSize;
	std::size_t uOffset;
	std::size_t vOffset;
	std::size_t colorDiffComponentSize;
	
	memLayout(uint16_t rgbWidth, uint16_t rgbHeight) {
		pixels = rgbWidth * rgbHeight;
		colorDiffComponentSize = pixels / 4 + (pixels % 4 != 0); // целое округленное в большую сторону
		rgbSize = pixels * rgb::pixelSize;
		frameSize = pixels + colorDiffComponentSize * 2;
		uOffset = 0 + pixels;
		vOffset = uOffset + colorDiffComponentSize;
	}
};

std::vector<raw> rgbToYuv420(const std::vector<raw>& rgb, const resolution& res);
std::vector<raw> yuv420ToRgb(const std::vector<raw>& yuv420, const resolution& res);

namespace simd {
	const int16_t simpl_YRatioR = static_cast<int16_t>(bt601::rgb2yuv::yRatioR * 256);
	const int16_t simpl_YRatioG = static_cast<int16_t>(bt601::rgb2yuv::yRatioG * 256);
	const int16_t simpl_YRatioB = static_cast<int16_t>(bt601::rgb2yuv::yRatioB * 256);
	const int16_t simpl_URatioR = static_cast<int16_t>(bt601::rgb2yuv::uRatioR * 256);
	const int16_t simpl_URatioG = static_cast<int16_t>(bt601::rgb2yuv::uRatioG * 256);
	const int16_t simpl_URatioB = static_cast<int16_t>(bt601::rgb2yuv::uRatioB * 256);
	const int16_t simpl_VRatioR = static_cast<int16_t>(bt601::rgb2yuv::vRatioR * 256);
	const int16_t simpl_VRatioG = static_cast<int16_t>(bt601::rgb2yuv::vRatioG * 256);
	const int16_t simpl_VRatioB = static_cast<int16_t>(bt601::rgb2yuv::vRatioB * 256);
	
	const uint8_t reverseConversionRShift = 8; // смещение для компенсации умножения на 256

	std::vector<raw> rgbToYuv420(const std::vector<raw>& rgb, const resolution& res);
}


} // namespace convert


inline image imageReservedForRgb(uint16_t width, uint16_t height) {
	convert::memLayout layout(width, height);
	return image{
		.data = std::vector<raw>(layout.rgbSize),
		.res = {width, height}
	};
}

inline image imageReservedForYuv(uint16_t width, uint16_t height) {
	convert::memLayout layout(width, height);
	return image{
		.data = std::vector<raw>(layout.frameSize),
		.res = {width, height}
	};
}


image bmp::readFile(const std::string& file, bool swap_RnB) {
	std::ifstream bmpFile(file, std::ios::binary);
	if (!bmpFile.is_open()) {
		throw std::runtime_error("Не удалось открыть файл: " + file);
	}
	
	std::array<char, bmp::HEADER_SIZE> header{};
	bmpFile.read(header.data(), header.size());
	
	if (bmp::depth(header.data()) != 24) {
		throw std::runtime_error("Это не 24битный BMP");
	}
	
	if (::fileSize(file.c_str()) != bmp::fileSize(header.data())) {
		throw std::runtime_error(".bmp файл повреждён");
	}
	
	std::size_t shiftToData = bmp::dataOffset(header.data()) - header.size();
	bmpFile.seekg(shiftToData, std::ifstream::cur);
	
	image img = imageReservedForRgb(static_cast<uint16_t>(bmp::width(header.data())),
		                        static_cast<uint16_t>(bmp::height(header.data())));
	
	const int rowPadded = (bmp::width(header.data()) * 3 + 3) & (~3);
	const std::size_t rowLength = bmp::width(header.data()) * rgb::pixelSize;
	const auto paddedBytes = static_cast<uint8_t>(rowPadded - rowLength);
	for (std::size_t writeOffset(0), i(bmp::height(header.data())); i > 0; i--) {
		writeOffset = i * rowLength - rowLength;
		bmpFile.read(reinterpret_cast<char *>(img.data.data() + writeOffset), rowLength);
		if (paddedBytes) {
			bmpFile.seekg(paddedBytes, std::ifstream::cur);
		}
	}
	
	// B, G, R  ->  R, G, B
	if (swap_RnB) {
		raw temp = 0;
		for (std::size_t i = 0; i <= img.data.size() - rgb::pixelSize; i += rgb::pixelSize) {
			assert(i + 2 < img.data.size());
			temp = img.data[i];
			img.data[i] = img.data[i + 2];
			img.data[i + 2] = temp;
		}
	}
	
	return img;
}


std::vector<raw> convert::rgbToYuv420(const std::vector<raw>& rgb, const resolution& res) {
	using namespace bt601;
	
	memLayout yuvLayout(res.width, res.height);
	
	std::vector<raw> yuv420(yuvLayout.frameSize);
	
	for (size_t pixel(0), it_y(0), it_u(yuvLayout.uOffset), it_v(yuvLayout.vOffset), it_row(0); pixel < yuvLayout.pixels; pixel++) {
		const raw *p = rgb.data() + pixel * rgb::pixelSize;
		yuv420[it_y++] = rgb2yuv::Y(rgb::getR(p), rgb::getG(p), rgb::getB(p));
		
		if ((it_row % 2) && (pixel % 2 == 0)) {
			assert(it_v < rgb.size());
			yuv420[it_u++] = rgb2yuv::U(rgb::getR(p), rgb::getG(p), rgb::getB(p));
			yuv420[it_v++] = rgb2yuv::V(rgb::getR(p), rgb::getG(p), rgb::getB(p));
		}
		
		if ((pixel > 0) && (pixel % res.width == 0)) {
			++it_row;
		}
	}
	return yuv420;
}


std::vector<raw> convert::simd::rgbToYuv420(const std::vector<raw>& rgb, const resolution& res) {
	using namespace bt601;
	using namespace rgb2yuv;
	
	std::size_t componentSize = rgb.size() / rgb::pixelSize;
	void *arr_r = aligned_alloc(16, 16 * componentSize);
	void *arr_g = aligned_alloc(16, 16 * componentSize);
	void *arr_b = aligned_alloc(16, 16 * componentSize);
	
	void *arr_y = aligned_alloc(16, 16 * componentSize);
	void *arr_u = aligned_alloc(16, 16 * componentSize);
	void *arr_v = aligned_alloc(16, 16 * componentSize);
	
	int16_t* pR = reinterpret_cast<int16_t*>(arr_r);
	int16_t* pG = reinterpret_cast<int16_t*>(arr_g);
	int16_t* pB = reinterpret_cast<int16_t*>(arr_b);
	
	int16_t* pY = reinterpret_cast<int16_t*>(arr_y);
	int16_t* pU = reinterpret_cast<int16_t*>(arr_u);
	int16_t* pV = reinterpret_cast<int16_t*>(arr_v);
	
	for(std::size_t it_rgb(0), it_cmpnt(0); it_rgb < rgb.size(); it_rgb += rgb::pixelSize) {
		pR[it_cmpnt] = static_cast<int16_t>(rgb[it_rgb]);
		pG[it_cmpnt] = static_cast<int16_t>(rgb[it_rgb + 1]);
		pB[it_cmpnt] = static_cast<int16_t>(rgb[it_rgb + 2]);
		it_cmpnt++;
	}
	
//	std::cout << "RGB-компонентные массивы: " << std::endl;
//	for(std::size_t i(0); i < componentSize; i++) {
//		std::cout << "R[" << +i << "]: " << pR[i]
//			<< "  G[" << +i << "]: " << pG[i]
//			<< "  B[" << +i << "]: " << pB[i]
//			<< std::endl;
//	}
	
	__m128i* simd_r = reinterpret_cast<__m128i*>(arr_r);
	__m128i* simd_g = reinterpret_cast<__m128i*>(arr_g);
	__m128i* simd_b = reinterpret_cast<__m128i*>(arr_b);
	
	__m128i* simd_y = reinterpret_cast<__m128i*>(arr_y);
	__m128i* simd_u = reinterpret_cast<__m128i*>(arr_u);
	__m128i* simd_v = reinterpret_cast<__m128i*>(arr_v);
	
	// умножаем константные float на 256 и приводим к int16 для избавление от операций с плавающей запятой
	const __m128i simd_ratio_yr = _mm_set1_epi16(simpl_YRatioR);
	const __m128i simd_ratio_yg = _mm_set1_epi16(simpl_YRatioG);
	const __m128i simd_ratio_yb = _mm_set1_epi16(simpl_YRatioB);
	const __m128i simd_ratio_ur = _mm_set1_epi16(simpl_URatioR);
	const __m128i simd_ratio_ug = _mm_set1_epi16(simpl_URatioG);
	const __m128i simd_ratio_ub = _mm_set1_epi16(simpl_URatioB);
	const __m128i simd_ratio_vr = _mm_set1_epi16(simpl_VRatioR);
	const __m128i simd_ratio_vg = _mm_set1_epi16(simpl_VRatioG);
	const __m128i simd_ratio_vb = _mm_set1_epi16(simpl_VRatioB);
	
	__m128i simd_128 = _mm_set1_epi16(128);
	
	__m128i simd_calced_y;
	__m128i simd_calced_u;
	__m128i simd_calced_v;
	
	// тут вычисляем yuv как для 4:4:4
	for (std::size_t i(0); i < componentSize; i++) {
		__m128i tmpYR = _mm_mullo_epi16(simd_ratio_yr, simd_r[i]);
		__m128i tmpYG = _mm_mullo_epi16(simd_ratio_yg, simd_g[i]);
		__m128i tmpYB = _mm_mullo_epi16(simd_ratio_yb, simd_b[i]);
		
		__m128i tmpUR = _mm_mullo_epi16(simd_ratio_ur, simd_r[i]);
		__m128i tmpUG = _mm_mullo_epi16(simd_ratio_ug, simd_g[i]);
		__m128i tmpUB = _mm_mullo_epi16(simd_ratio_ub, simd_b[i]);
		
		__m128i tmpVR = _mm_mullo_epi16(simd_ratio_vr, simd_r[i]);
		__m128i tmpVG = _mm_mullo_epi16(simd_ratio_vg, simd_g[i]);
		__m128i tmpVB = _mm_mullo_epi16(simd_ratio_vb, simd_b[i]);
		
		__m128i tmpY = _mm_add_epi16(tmpYR, _mm_add_epi16(tmpYG, tmpYB));
		__m128i tmpU = _mm_add_epi16(tmpUR, _mm_add_epi16(tmpUG, tmpUB));
		__m128i tmpV = _mm_add_epi16(tmpVR, _mm_add_epi16(tmpVG, tmpVB));
		
		simd_calced_y = _mm_srai_epi16(tmpY, reverseConversionRShift);
		simd_calced_u = _mm_add_epi16(_mm_srai_epi16(tmpU, reverseConversionRShift), simd_128);
		simd_calced_v = _mm_add_epi16(_mm_srai_epi16(tmpV, reverseConversionRShift), simd_128);
		
		_mm_store_si128(simd_y++, simd_calced_y);
		_mm_store_si128(simd_u++, simd_calced_u);
		_mm_store_si128(simd_v++, simd_calced_v);
	}
	
	memLayout yuvLayout(res.width, res.height);
	std::vector<raw> yuv420(yuvLayout.frameSize);
	
	// проредить yuv до 4:2:0
	for (size_t pixel(0), it_y(0), it_u(yuvLayout.uOffset), it_v(yuvLayout.vOffset), it_row(0), i(0); pixel < yuvLayout.pixels; pixel++) {
		const raw *p = rgb.data() + pixel * rgb::pixelSize;
		yuv420[it_y++] = pY[i];
		
		if ((it_row % 2) && (pixel % 2 == 0)) {
			assert(it_v < rgb.size());
			yuv420[it_u++] = pU[i];
			yuv420[it_v++] = pV[i];
		}
		
		if ((pixel > 0) && (pixel % res.width == 0)) {
			++it_row;
		}
		i++;
	}
	
	
	free(arr_r);
	free(arr_g);
	free(arr_b);
	
	free(arr_y);
	free(arr_u);
	free(arr_v);
	return yuv420;
}



std::vector<raw> convert::yuv420ToRgb(const std::vector<raw>& yuv420, const resolution& res) {
	using namespace bt601;
	
	memLayout yuvLayout(res.width, res.height);
	
	std::vector<raw> rgb(yuvLayout.rgbSize);
	
	// row и pos считаются от нуля
	auto convertPixel = [&yuv420, &rgb, &yuvLayout, &res](std::size_t row, std::size_t pos, std::size_t it_uv){
		std::size_t itPixel = row * res.width + pos;
		std::size_t it_y = itPixel;
		std::size_t it_u = yuvLayout.uOffset + it_uv;
		std::size_t it_v = yuvLayout.vOffset + it_uv;
		
		auto itByte = itPixel * rgb::pixelSize;
		assert(itByte + 2 < rgb.size());
		assert(it_v < yuv420.size());
		
		rgb[itByte]     = yuv2rgb::R(yuv420[it_y], yuv420[it_v]);
		rgb[itByte + 1] = yuv2rgb::G(yuv420[it_y], yuv420[it_u], yuv420[it_v]);
		rgb[itByte + 2] = yuv2rgb::B(yuv420[it_y], yuv420[it_u]);
	};
	
	for(std::size_t it_uv(0), posInRow(0), row(0); row < res.height;) {
		convertPixel(row, posInRow, it_uv);
		if (row + 1 <= res.height) {
			convertPixel(row + 1, posInRow, it_uv);
		}
		
		if(posInRow % 2 != 0) {
			it_uv++;
		}
		if (posInRow == res.width - 1) {
			posInRow = 0;
			row +=2;
		} else {
			posInRow++;
		}
	}
	
	return rgb;
}


void imgOverlay(std::vector<raw>& backImg, resolution backRes,
                const std::vector<raw>& frontImg, resolution frontRes,
                uint16_t horizShift,
                uint16_t vertShift) {
	const std::size_t posRow = backRes.width * vertShift * rgb::pixelSize;
	const std::size_t posOverlayInRow = horizShift * rgb::pixelSize;
	const uint16_t overlayWidth = backRes.width >= frontRes.width + horizShift
					? frontRes.width
					: backRes.width - horizShift;
	const uint16_t overlayHeigth = backRes.height >= frontRes.height + vertShift
					? frontRes.height
					: backRes.height - vertShift;
	
	for (std::size_t posCurrentRow(posRow), srcRow(0); srcRow < overlayHeigth; srcRow++) {
		std::size_t posOverlay = posCurrentRow + posOverlayInRow;
		std::size_t posSrc = srcRow * frontRes.width * rgb::pixelSize;
		std::uint16_t copySize = overlayWidth * rgb::pixelSize;

		assert(posSrc + copySize <= frontImg.size());
		assert(posOverlay + copySize <= backImg.size());
		
		memcpy(backImg.data() + posOverlay, frontImg.data() + posSrc, copySize);
		posCurrentRow += backRes.width * rgb::pixelSize;
	}
}


void dump(const std::vector<raw> &data, const std::string& fileName, const std::string& description) {
	std::cout << description << ": " << fileName << std::endl;
	std::ofstream file(fileName, std::ofstream::binary);
	file.write(reinterpret_cast<const char *>(data.data()), data.size());
	file.close();
}


void processFrame(std::promise<std::vector<raw>> promObj, const image& backImg, const image& frontImg, uint16_t overlayShiftHoriz, uint16_t overlayShiftVert, bool dumpTmp = false) {
	auto backgroundRGB = convert::yuv420ToRgb(backImg.data, backImg.res);
	if (dumpTmp) {
		dump(backgroundRGB, "background.rgb", "Пишем временный background конвертированный в RGB");
	}
	
	imgOverlay(backgroundRGB, backImg.res, frontImg.data, frontImg.res, overlayShiftHoriz, overlayShiftVert);
	if (dumpTmp) {
		dump(backgroundRGB, "summary.rgb", "Пишем картинку наложенную на картинку");
	}
	
	//auto yuvFrame = convert::rgbToYuv420(backgroundRGB, backImg.res);
	auto yuvFrame = convert::simd::rgbToYuv420(backgroundRGB, backImg.res);
	if (dumpTmp) {
		dump(yuvFrame, "frame.yuv420", "Исходное изображение yuv с наложенной bmp-картинкой");
	}
	promObj.set_value(yuvFrame);
}


struct threadData {
	std::future<std::vector<raw>> futureResult;
	std::thread thread;
	
	threadData(const image& backImg, const image& frontImg, uint16_t overlayShiftHoriz, uint16_t overlayShiftVert)
	{
		std::promise<std::vector<raw>> promiseObj;
		futureResult = promiseObj.get_future();
		thread = std::thread(
			processFrame,
			std::move(promiseObj),
			backImg,
			frontImg,
			overlayShiftHoriz,
			overlayShiftVert,
			false
		);
	};
};


const std::string arg_h("-h");
const std::string arg_help("--help");
const std::string arg_dumpTmp("--dbgdump");
const std::string arg_threads("--j");
const std::string progName("bmp_over_yuv");
const uint8_t positional_args = 5;
const uint8_t threadNumDefault = 2;


void showUsage() {
	std::cout << "Использование:" << std::endl;
	std::cout << "  " << progName << "  ФАЙЛ_картинка_yuv420  ШИРИНА  ВЫСОТА  ФАЙЛ_картинка.bmp  [ПАРАМЕТРЫ]" << std::endl;
	std::cout << std::endl;
	std::cout << "Параметры:" << std::endl;
	std::cout << "  " << arg_threads << " \t\tзадать число тредов" << std::endl;
	std::cout << "  " << arg_dumpTmp << " \tdump временных картинок для отладки" << std::endl;
}


int main(int argc, char **argv) {
	if (argc < positional_args + 1) {
		std::cout << "Неверные аргументы" << std::endl;
		showUsage();
		return EXIT_FAILURE;
	}
	
	if (arg_h == argv[1] || arg_help == argv[1]) {
		showUsage();
		return EXIT_FAILURE;
	}
	
	bool dumpTmp(false);
	uint8_t threadNum = threadNumDefault ;
	for (int i = positional_args + 1; i < argc; i++) {
		if (arg_dumpTmp == argv[i]) {
			dumpTmp = true;
		} if (arg_threads == argv[i]) {
			threadNum = static_cast<uint8_t>(atoi(argv[++i]));
		} else {
			std::cout << "Не известный аргумент: \"" << argv[i] << "\"" << std::endl;
			showUsage();
			return EXIT_FAILURE;
		}
	}
	std::cout << "Будет использовано " << +threadNum << " потоков обработки" << std::endl;
	if (dumpTmp) {
		std::cout << "Включен dump временных изображений" << std::endl;
	}
	
	image frameImg = imageReservedForYuv(static_cast<uint16_t>(std::stoul(argv[2])),
		                             static_cast<uint16_t>(std::stoul(argv[3])));
	
	try {
		const auto frontImg = bmp::readFile(argv[4], true);
		if (dumpTmp) {
			dump(frontImg.data, "frontImg.rgb", "Пишем прочитанные RGB-данные");
		}
		
		if (frameImg.res.width < frontImg.res.width || frameImg.res.height < frontImg.res.height) {
			std::cout << "Накладываемое изображение должно быть меньше или равно исходному" << std::endl;
			return EXIT_FAILURE;
		}
		
		std::ifstream inFile(argv[1], std::fstream::binary);
		std::ofstream outFile(argv[5], std::fstream::binary);
		
		std::size_t videoSize = fileSize(argv[1]);
		if (videoSize % frameImg.data.size() != 0) {
			std::cout << "С видеофайлом что-то не так. Число фреймов "
				<< "[ " << videoSize << "(fileSize) / " << frameImg.data.size() << "(frameSize) ]"
				<< " = " << std::setprecision(std::numeric_limits<double>::max_digits10)
				<< static_cast<double>(videoSize) / frameImg.data.size() << std::endl;
		}
		const std::size_t framesNum = videoSize / frameImg.data.size();
		
		const uint16_t overlayShiftH = frameImg.res.width - frontImg.res.width;
		const uint16_t overlayShiftV = frameImg.res.height - frontImg.res.height;
		
		std::queue<threadData> threadHandlers;
		for(uint16_t frame(1); !inFile.eof();) {
			for(uint8_t i(0); i < threadNum; frame++, i++) {
				inFile.read(reinterpret_cast<char *>(frameImg.data.data()), frameImg.data.size());
				if (inFile.gcount() < frameImg.data.size()) {
					break;
				}
				
				std::cout << "Обработка фрейма " << +frame << " из " << framesNum << std::endl;
				threadHandlers.emplace(
					threadData(frameImg, frontImg, overlayShiftH, overlayShiftV)
				);
			}
			
			while (!threadHandlers.empty()) {
				auto yuvFrame = threadHandlers.front().futureResult.get();
				threadHandlers.front().thread.join();
				outFile.write(reinterpret_cast<const char *>(yuvFrame.data()), yuvFrame.size());
				threadHandlers.pop();
			}
		}
		
		inFile.close();
		outFile.close();
	} catch(std::runtime_error& e) {
		std::cout << "Ошибка: " << e.what() << std::endl;
	}
	
	std::cout << "Завершено" << std::endl;
	return EXIT_SUCCESS;
}
